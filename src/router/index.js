import Vue from 'vue'
import Router from 'vue-router'
import Clue from '@/components/Clue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'First-Clue',
      component: Clue
    },
    {
      path: '/:code',
      name: 'Clue',
      component: Clue,
      props: true
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
